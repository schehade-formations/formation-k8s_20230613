# TP 5 - Deployment

```bash
mkdir ../TP05 && cd ../TP05
kubectl create deployment my-app --image=samiche92/my-node-server:1.0.0 -o yaml --dry-run=client > my-app.yaml
vim my-app.yaml
```

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: my-app
  annotations:
    kubernetes.io/change-cause: "v1"
spec:
  replicas: 2
  selector:
    matchLabels:
      app: backend
  template:
    metadata:
      labels:
        app: backend
    spec:
      containers:
      - image: samiche92/my-node-server:1.0.0
        name: my-ctn
```

```bash
kubectl delete rs my-rs
kubectl apply -f my-app.yaml 
kubectl get deployments,replicasets,pods
kubectl rollout history deployment my-app

vim my-app.yaml
# kubernetes.io/change-cause: "v1" => "v2"
# image: samiche92/my-node-server:1.0.0 => 2.0.0
kubectl get rs -w
# Dans un 2nd terminal
kubectl get pods -w
# Dans un 3e terminal
kubectl apply -f my-app.yaml
kubectl rollout history deployment my-app
kubectl get deployment my-app -o yaml | less
kubectl explain deployment.spec.strategy.rollingUpdate
kubectl rollout undo deployment my-app --to-revision=1
kubectl rollout history deployment my-app
```
