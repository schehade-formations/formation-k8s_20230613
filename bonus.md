# TP Bonus

```bash
kubectl cluster-info
kubectl get pods -v 9
curl https://192.168.49.2:8443 -k
kubectl proxy
curl localhost:8001
curl localhost:8001/apis/batch
curl localhost:8001/apis/batch/v1
cd ../TP13/
kubectl apply -f my-job.yaml
curl localhost:8001/apis/batch/v1/jobs
curl localhost:8001/apis/batch/v1/namespaces/default/jobs/my-job | less

kubectl get serviceaccounts
kubectl describe pod my-pod | grep "Service Account:"
```

```bash
sudo dnf install helm
helm create my-app
kubectl create configmap my-cm --from-literal=DELAY=1s -o yaml --dry-run=client > my-app/templates/my-cm.yaml
rm my-app/templates/deployment.yaml 
rm my-app/templates/hpa.yaml 
rm my-app/templates/ingress.yaml 
rm my-app/templates/serviceaccount.yaml 
rm my-app/templates/service.yaml
rm -r my-app/templates/tests/
kubectl delete cm --all
helm install my-chart ./my-app/
helm uninstall my-chart
vim my-app/values.yaml
```

```yaml
delay: 2
```

```bash
cp my-app/values.yaml my-app/values-prod.yaml 
vim my-app/values-prod.yaml
# delay: 2 => delay: 5
vim my-app/templates/my-cm.yaml
```

```yaml
apiVersion: v1
data:
  DELAY: "{{ .Values.delay }}"
kind: ConfigMap
metadata:
  creationTimestamp: null
  name: my-cm
```

```bash
echo '' > my-app/templates/NOTES.txt
kubectl get cm
kubectl describe cm my-cm
helm uninstall my-chart
helm install my-chart ./my-app/
kubectl get cm
kubectl describe cm my-cm
helm uninstall my-chart
helm install my-chart ./my-app/ -f my-app/values-prod.yaml 
kubectl describe cm my-cm
helm install my-chart ./my-app/ -f my-app/values-prod.yaml --dry-run --debug
helm install my-chart ./my-app/ -f my-app/values.yaml --dry-run --debug
```
